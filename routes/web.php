<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', [
	'uses' => 'FeedsController@homepage',
	'as' => 'homepage'
]);

Route::any('/login', [
	'uses' => 'UsersController@login',
	'as' => 'login'
]);
Route::any('/logout', [
	'uses' => 'UsersController@logout',
	'as' => 'logout'
]);

Route::group(['middleware'=>'auth', 'prefix'=>'admin'], function() {
	Route::get('/', [
		'uses' => 'FeedsController@dashboard',
		'as' => 'admin.dashboard'
	]);
	Route::any('/changepassword', [
		'uses' => 'UsersController@changePassword',
		'as' => 'admin.changepassword'
	]);
	Route::resource('feeds', 'FeedsController');
	Route::resource('categories', 'CategoriesController');
});