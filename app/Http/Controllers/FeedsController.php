<?php

namespace App\Http\Controllers;

use App\Category;
use App\Feed;
use App\FeedProvider;
use Illuminate\Http\Request;

class FeedsController extends Controller
{
    public function homepage()
    {
        $categories = Category::get()->pluck('name', 'id');
        $feeds = Feed::limit(100)->orderBy('utc_time', 'desc');
        if (request()->input('filter')) {
            $category = request()->input('filter');
            $feeds->join('feeds_categories', function ($q) use ($category) {
                $q->whereRaw('feeds_categories.feed_id = feeds.provider_id');
                $q->where('feeds_categories.category_id', $category);
            });
        }
        $feeds = $feeds->get();
        return view('feeds.homepage', ['categories' => $categories, 'feeds'=>$feeds]);
    }



    // Admin methods

    public function dashboard()
    {
        return view('feeds.dashboard');
    }

    // Admin CRUD

    public function index()
    {
        if (Category::count() == 0) {
            return redirect()->route('categories.index')->with(
                ['message'=>'Before adding feed provider, please create atleast one category']
            );
        }

        $feeds = FeedProvider::get();

        return view('feeds.index', [ 'feeds' => $feeds]);
    }

    public function create()
    {
        $categories = Category::get()->pluck('name', 'id');
        return view('feeds.create', ['categories' => $categories]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'url' => 'required|url',
            'category' => 'required'
        ]);

        $feed = FeedProvider::create([
            'url' => $request->input('url')
        ]);
        $feed->categories()->sync($request->input('category'));
        return redirect()->route('feeds.index');
    }

    public function edit($id)
    {
        $categories = Category::get()->pluck('name', 'id');
        $feed = FeedProvider::where('id', $id)->first();
        $selectedCategories = $feed->categories()->get()->pluck('id')->toArray();
        return view('feeds.edit', [
            'feed' => $feed, 'categories' => $categories,
            'selectedCategories' => $selectedCategories
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'url' => 'required|url',
            'category' => 'required'
        ]);

        $feed = FeedProvider::where('id', $id)->first();
        $feed->fill($request->all())->save();
        $feed->categories()->sync($request->input('category'));

        return redirect()->route('feeds.index');
    }

    public function destroy($id)
    {
        $feed = FeedProvider::where('id', $id)->first();
        $feed->categories()->detach();
        $feed->feeds()->delete();
        $feed->delete();
        return redirect()->route('feeds.index');
    }
}
