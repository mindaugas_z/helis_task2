<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function login()
    {
        if (request()->isMethod('POST')) {
            if (!Auth::attempt([
                'username'=>request()->username,
                'password'=>request()->password
                ])
            ) {
                return redirect()->route('login');
            } else {
                return redirect()->route('admin.dashboard');
            }
        }
        return view('users.login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('homepage');
    }


    // Admin methods
    public function changePassword(Request $request)
    {
        if ($request->isMethod('POST')) {
            $this->validate($request, [
                'password' => 'required|confirmed'
            ]);
            Auth::user()->fill(['password' => $request->input('password')])->update();
            return redirect()->route('admin.dashboard');
        }
        return view('users.changepassword');
    }
}
