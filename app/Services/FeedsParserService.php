<?php

namespace App\Services;

class FeedsParserService
{
    protected $data;

    public function __construct()
    {
    }

    public function fetchFeed($url)
    {
        $this->data = file_get_contents($url);
    }

    public function isAtomFeed()
    {
        if (strpos($this->data, '<channel>') !== false && strpos($this->data, '<pubDate') !== false) {
            return true;
        }

        // if (strpos($this->data, 'http://www.w3.org/2005/Atom') !== false) {
        //     return true;
        // }

        return false;
    }

    public function parseAtomFeed()
    {
        $data = simplexml_load_string($this->data);
        $items = [];

        foreach ($data->channel->item as $item) {
            $title = (string) $item->title;
            if (empty($title)) {
                continue;
            }
            $items[] = (object)[
                'provider_title' => (string)$data->channel->title,
                'provider_url' => (string)$data->channel->link,
                'title' => $title,
                'description' => (string) $item->description,
                'url' => (string) $item->link,
                'date' => $this->fixDate((string) $item->pubDate)
            ];
        }

        return $items;
    }

    private function fixDate($value)
    {
        $months = ['Sau'=>'Jan','Kov'=>'Mar','Kov'=>'Mar','Bal'=>'Apr','Geg'=>'May','Bir'=>'Jun','Lie'=>'Jul','Rgp'=>'Aug','Rgs'=>'Sep','Spa'=>'Oct','Lap'=>'Nov','Grd'=>'Dec'];
        $days = ['Pr'=>'Mon','An'=>'Tue','Tr'=>'Wed','Kt'=>'Thu','Pn'=>'Fri','Št'=>'Sat','Sk'=>'Sun'];

        $value = str_replace(array_keys($months), array_values($months), $value);
        $value = str_replace(array_keys($days), array_values($days), $value);
        
        $date = new \DateTime($value);
        $date->setTimezone(new \DateTimeZone('UTC'));
        return $date->format('Y-m-d H:i:s');
    }
}
