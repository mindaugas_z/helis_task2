<?php

namespace App\Providers;

use App\Services\FeedsParserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('feed.parser', function($app) {
            return new FeedsParserService();
        });
    }
}
