<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    protected $fillable = [
        'provider_id', 'provider_name', 'provider_url',
        'feed_title', 'feed_description', 'feed_url', 'utc_time'
    ];

    protected $dates = ['utc_time'];

    protected $table = 'feeds';

    public function feedProvider()
    {
        return $this->belongsTo('App\FeedProvider', 'provider_id');
    }

    public function getUtcTimeAttribute($value)
    {
        $date = new \DateTime($value . ' +0000');
        $date->setTimezone(new \DateTimeZone(config('app.timezone')));
        return $date->format('Y-m-d H:i:s');
    }
}
