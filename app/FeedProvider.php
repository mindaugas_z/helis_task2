<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedProvider extends Model
{
    protected $fillable = ['url'];
    
    protected $table = 'feed_providers';

    public function feeds()
    {
        return $this->hasMany('App\Feed', 'provider_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'feeds_categories', 'feed_id', 'category_id');
    }
}
