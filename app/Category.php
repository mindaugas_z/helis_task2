<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name'];

    protected $table = 'categories';

    public function feedProviers()
    {
        return $this->belongsToMany('App\FeedProvider', 'feeds_categories', 'category_id', 'feed_id');
    }
}
