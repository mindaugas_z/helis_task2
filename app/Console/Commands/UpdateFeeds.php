<?php

namespace App\Console\Commands;

use App\Feed;
use App\FeedProvider;
use Illuminate\Console\Command;

class UpdateFeeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $feedProviders = FeedProvider::get();

        $feedParser = app('feed.parser');

        foreach ($feedProviders as $fp) {
            $this->info('Parsing '.$fp->url);
            
            $feedParser->fetchFeed($fp->url);
            $items = [];

            if ($feedParser->isAtomFeed()) {
                $items = $feedParser->parseAtomFeed();
            }

            if (count($items) == 0) {
                $this->error('Something went wrong. skiping');
                continue;
            }

            foreach ((array) $items as $item) {
                try {
                    @Feed::create([
                        'provider_id' => $fp->id,
                        'provider_name' => $item->provider_title,
                        'provider_url' => $item->provider_url,
                        'feed_title' => $item->title,
                        'feed_description' => $item->description,
                        'feed_url' => $item->url,
                        'utc_time' => $item->date
                    ]);
                } catch (\PDOException $e) {
                }
            }
            $this->info('Done');
        }
    }
}
