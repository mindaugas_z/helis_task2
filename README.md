task: 

	https://bitbucket.org/Sauls/seohelis-devdocs/wiki/new-team-member/backend/task2.md

setup:

	run: git clone https://mindaugas_z@bitbucket.org/mindaugas_z/helis_task2.git helis_task2

	run: cd helis_task2

	run: composer install

	run: cp .env.example .env

	run: touch database/database.sqlite

	update: set DB_CONNECTION variable in .env file to "sqlite" and delete all other params with prefix DB_

	run: php artisan migrate

	run: php artisan key:generate

	run: php artisan user:create

	run: php artisan serve, go to web page, add some rss feeds eg  http://www.elektronika.lt/rss/naujienos/ http://www.delfi.lt/rss/feeds/daily.xml then back to command line and run: php artisan feed:update

