@extends('layouts.admin')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		Edit feed provider
	</div>
	<div class="panel-body">
		@include('partials.errors')
		
		<form action="{{ route('feeds.update', ['feed'=>$feed->id]) }}" method="POST">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
			<div class="form-group">
				<label for="url">Feed provider rss feed url</label>
				<input value="{{ $feed->url }}" type="text" name="url" class="form-control" placeholder="http://www.delfi.lt/rss/feeds/daily.xml" required>
			</div>
		
			<div class="form-group">
				@foreach($categories as $id=>$name)
					<input type="checkbox" name="category[]" value="{{ $id }}" {{ in_array($id, $selectedCategories)?"checked":"" }}> {{ $name }}
				@endforeach
			</div>
			

			<button type="submit" class="btn btn-primary">Save</button>
			<a href="{{ route('feeds.index') }}" class="btn btn-warning">Cancel</a>
		</form>
	</div>
</div>
@endsection