@extends('layouts.admin')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		Feed providers
	</div>
	<div class="panel-body">

		<a class="btn btn-primary btn-xs" href="{{ route('feeds.create') }}">Create</a>

		<table class="table">
			<tr>
				<th>Url</th>
				<th style="width: 100px;">Actions</th>
			</tr>

			@foreach($feeds as $feed)
			<tr>
				<td>{{ $feed->url }}</td>
				<td>
					<form action="{{ route('feeds.destroy', ['feed'=>$feed->id]) }}" method="POST">
						<a class="btn btn-primary btn-xs" href="{{ route('feeds.edit', ['feed'=>$feed->id]) }}">edit</a>
						{{ csrf_field() }} {{ method_field('DELETE') }}
						<button class="btn btn-danger btn-xs" type="submit">delete</button>
					</form>
			@endforeach

		</table>
	</div>
</div>
@endsection