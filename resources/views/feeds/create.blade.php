@extends('layouts.admin')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		Create feed provider
	</div>
	<div class="panel-body">
		
		@include('partials.errors')

		<form action="{{ route('feeds.store') }}" method="POST">
			{{ method_field('POST') }}
			{{ csrf_field() }}
			<div class="form-group">
				<label for="url">Feed provider rss feed url</label>
				<input type="text" value="{{ old('url') }}" name="url" class="form-control" placeholder="http://www.delfi.lt/rss/feeds/daily.xml" required>
			</div>
			<div class="form-group">
				@foreach($categories as $id=>$name)
					<input type="checkbox" name="category[]" value="{{ $id }}"> {{ $name }}
				@endforeach
			</div>
			<button type="submit" class="btn btn-primary">Create</button>
			<a href="{{ route('feeds.index') }}" class="btn btn-warning">Cancel</a>
		</form>		
	</div>
</div>
@endsection