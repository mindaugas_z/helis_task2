@extends('layouts.app')

@section('content')
<script>
	function showModal(id) {
		$('#myModal .modal-link').attr('href', $('#'+id+' a').attr('href'));
		$('#myModal .modal-title').html($('#'+id+' a').html());
		$('#myModal .modal-body').html($('#'+id+' div').html());
		$('#myModal').modal();
	}
</script>

<div class="container">
		<div class="row">
			<div class="col-md-2">
				<div class="panel panel-default">
					<div class="panel-heading">
						Categories
					</div>	
					<div class="panel-body">
						<a href="{{ route('homepage') }}">All feeds</a><br/>
						@foreach($categories as $id=>$name)
							<a href="{{ route('homepage', ['filter'=>$id]) }}">{{ $name }}</a><br/>
						@endforeach
					</div>
				</div>
			</div>
			<div class="col-md-10">
				<div class="panel panel-default">
					<div class="panel-heading">
						RSS Feeds
					</div>
					<div class="panel-body">
					<!-- Button trigger modal -->
						<table class="table">
							@foreach($feeds as $feed)
								<tr>
									<td style="min-width: 200px;">
										<a target="_blank" href="{{ $feed->provider_url }}">{{ $feed->provider_name }}</a>
									</td>
									<td id="feed-{{ $feed->id }}">
										<a onclick="showModal('feed-{{$feed->id}}'); return false;" target="_blank" href="{{ $feed->feed_url }}">{{ $feed->feed_title }}</a>
										<div style="display: none;">
											{{ $feed->feed_description }}
										</div>
									</td>
									<td style="width: 150px;">
										{{ $feed->utc_time }}
									</td>
								</tr>
							@endforeach

						</table>
					</div>
				</div>
			</div>
		</div>
	</div>


		<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a href="" class="btn btn-primary modal-link" target="_blank">Go to feed page</a>
      </div>
    </div>
  </div>
</div>




@endsection