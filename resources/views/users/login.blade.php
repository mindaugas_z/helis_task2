@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					Login
				</div>
				<div class="panel-body">
					<form action="" method="POST">
						{{ csrf_field() }}
						<div class="form-group">
							<input type="text" name="username" placeholder="username" class="form-control" required>
						</div>
						<div class="form-group">
							<input type="password" name="password" placeholder="password" class="form-control" required>
						</div>

						<button class="btn btn-primary" type="submit">Login</button>
					</form>
				</div>

			</div>
		</div>
	</div>
</div>
@endsection