@extends('layouts.admin')

@section('content')
<div class="panel panel-default">
	<div class="panel-heading">
		Change password
	</div>
	<div class="panel-body">

		@include('partials.errors')

		<form action="" method="POST">
			{{ csrf_field() }}
			<div class="form-group">
				<input type="text" name="password" class="form-control" placeholder="New password" required>
			</div>
			<div class="form-group">
				<input type="text" name="password_confirmation" class="form-control" placeholder="Repeat new password" required>
			</div>

			<button class="btn btn-primary">
				Change
			</button>

		</form>
	</div>

</div>
@endsection