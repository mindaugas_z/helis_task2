@extends('layouts.admin')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		Create category
	</div>
	<div class="panel-body">
		@include('partials.errors')
		
		<form action="{{ route('categories.store') }}" method="POST">
			{{ csrf_field() }}
			<div class="form-group">
				<label for="">Category name</label>
				<input value="{{ old('name') }}" type="text" class="form-control" name="name" placeholder="Name" required>
			</div>

			<button class="btn btn-primary" type="submit">Create</button>
			<a href="{{ route('categories.index') }}" class="btn btn-warning">Cancel</a>

		</form>
	</div>
</div>

@endsection