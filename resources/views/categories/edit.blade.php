@extends('layouts.admin')

@section('content')

<div class="panel panel-default">
	<div class="panel-heading">
		Create category
	</div>
	<div class="panel-body">

		@include('partials.errors')

		<form action="{{ route('categories.update', ['category'=>$category->id]) }}" method="POST">
			{{ method_field('PUT') }}
			{{ csrf_field() }}
			<div class="form-group">
				<label for="">Category name</label>
				<input value="{{ $category->name }}" type="text" class="form-control" name="name" placeholder="Name" required>
			</div>

			<button class="btn btn-primary" type="submit">Save</button>
			<a href="{{ route('categories.index') }}" class="btn btn-warning">Cancel</a>

		</form>
	</div>
</div>

@endsection