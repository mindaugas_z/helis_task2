@extends('layouts.admin')

@section('content')
<div class="panel-heading">
	Categories
</div>
<div class="panel-body">

	@include('partials.message')

	<a href="{{ route('categories.create') }}" class="btn btn-primary btn-xs">Create</a>

	<table class="table">
		<tr>
			<th>Name</th>
			<th style="width: 100px;">Actions</th>
		</tr>

		@foreach($categories as $category)
		<tr>
			<td>{{ $category->name }}</td>
			<td>
				
				<form class="form-inline" action="{{ route('categories.destroy', ['category'=>$category->id]) }}" method="post">
				<a class="btn btn-primary btn-xs" href="{{ route('categories.edit', ['category'=>$category->id])}}">edit</a>
					{{ csrf_field() }} {{ method_field('DELETE') }}
					<button type="submit" class="btn btn-danger btn-xs">delete</button>
				</form>
			</td>
		</tr>
		@endforeach

	</table>
</div>
@endsection